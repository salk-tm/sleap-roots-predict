# sleap-roots-predict


## Description
The `main` function (found in "src/main.py") gets SLEAP predictions for each scan and each model type. It expects three positional arguments. 

```
python src/main.py <images_input_dir> <models_input_dir> <output_dir>
```

- This function expects each scan to have 72 images. This is hardcoded now but can be easilly changed in `predict.video_from_scan`.

## Arguments

- **`images_input_dir`**: The directory containing the output of the container `images-downloader`.
  - **images**: Images are downloaded using the Bloom CLI.
  - **`scans.csv`**: Metadata from Bloom and the scan path of each scan. Important columns for this code are "scan_path" and "scan_id".
  - **`dataset_params.json`**: Parameters for filtering `scans.csv` to only get predictions for sample within criteria. This should always be included. 
  Ex:
```
  {
    "species_name": "Rice",
    "min_wave": 1,
    "max_wave": 1,
    "min_age": 3,
    "max_age": 3
}
```

- **`models_input_dir`**: The directory containing the output of the container `models-downloader`.
  - **zipped chosen models**: Models choosen by user parameters and model chooser table are copied as zip files.
  - **`model_paths.csv`**: CSV of `model_type`, `model_id`, `model_path` corresponding to root type, *unique model id from Bloom in the future* and path of the model at it's destination. 

- **`output_dir`**: The directory where the predictions are output.
  - **predictions**: Each scan and model type has one prediction saved with name in the format "scan{scan_id}_model{model_id}_{model_type}.slp". This is a SLEAP file that can be loaded as a SLEAP `Labels` object using `Labels.load_file("<prediction_path>")`.
  - **`predictions.csv`**: Original `scans.csv` downloaded from Bloom merged with prediction path info.


## Badges
[![pipeline status](https://gitlab.com/salk-tm/sleap-roots-predict/badges/main/pipeline.svg)](https://gitlab.com/salk-tm/sleap-roots-predict/-/commits/main)

[![coverage report](https://gitlab.com/salk-tm/sleap-roots-predict/badges/main/coverage.svg)](https://gitlab.com/salk-tm/sleap-roots-predict/-/commits/main)

## Installation

**Make sure to have Docker Desktop running first**


You can pull the image if you don't have it built locally, or need to update the latest, with

```
docker pull registry.gitlab.com/salk-tm/sleap-roots-predict:latest
```

## Usage 

Then, to run the image with gpus interactively:

```
docker run --gpus all -it registry.gitlab.com/salk-tm/sleap-roots-predict:latest bash
```

and test with 

```
python -c "import sleap; print(sleap.version)" && nvidia-smi
```

To run the image interactively with data mounted to the container, use the syntax

```
docker run -v /path/on/host:/path/in/container [other options] image_name [command]
```

Note that host paths are absolute. 

For example (Windows syntax): 

```
docker run -v "C:\repos\sleap-roots-predict\tests\data\images-downloader-output\round1_2021":/workspace/images_input ^
    -v "C:\repos\sleap-roots-predict\tests\data\models-downloader-output":/workspace/models_input ^
    -v "C:\repos\sleap-roots-predict\tests\data\sleap-roots-predict-output":/workspace/output ^
    --gpus all -it registry.gitlab.com/salk-tm/sleap-roots-predict:latest bash
```

check the folders are mounted with `ls`. 

Then you can run the `main` function in the /workspace directory in the container using

```
python src/main.py ./images_input ./models_input ./output 
```

and check "/workspace/output" for the expected files (also available on the mounted host output folder).

This can all be run via the command line as well. For example:


```
docker run -v "C:\Users\eb\Desktop\root_phenotyping\sleap-roots-pipeline-dir\sleap-roots-predict\images-downloader-input\round1_2021":/workspace/images_input ^
    -v "C:\Users\eb\Desktop\root_phenotyping\sleap-roots-pipeline-dir\sleap-roots-predict\models-downloader-input":/workspace/models_input ^
    -v "C:\Users\eb\Desktop\root_phenotyping\sleap-roots-pipeline-dir\sleap-roots-predict\output":/workspace/output ^
    --gpus all -it registry.gitlab.com/salk-tm/sleap-roots-predict:latest ^
    python src/main.py ./images_input ./models_input ./output
```

## Contributing

- Use the `devcontainer.json` to open the repo in a dev container using VS Code.
  - There is some test data in the `tests` directory that will be automatically mounted for use since the working directory is the workspace.
  - You can test your modifications to `main` with something like
  ```
  python src/main.py ./tests/data/images-downloader-output/round1_2021 ./tests/data/models-downloader-output ./tests/data/sleap-roots-predict-output
  ```
    using the terminal in the container. 
  - Rebuild the container when you make changes using `Dev Container: Rebuild Container`.

- Please make a new branch, starting with your name, with any changes, and request a reviewer before merging with the main branch since this container will be used by all HPI.
- Please document using the same conventions (docstrings for each function and class, typing-hints, informative comments).
- Tests are written in the pytest framework. Data used in the tests are defined as fixtures in "tests/fixtures/data.py" ("https://docs.pytest.org/en/6.2.x/reference.html#fixtures-api").

**Notes:**

- The `registry.gitlab.com` is the Docker registry where the images are pulled from. This is only used when pulling images from the cloud, and not necesary when building/running locally.
- `-it` ensures that you get an interactive terminal. The `i` stands for interactive, and `t` allocates a pseudo-TTY, which is what allows you to interact with the bash shell inside the container.
- The `-v` or `--volume` option mounts the specified directory with the same level of access as the directory has on the host.
- `bash` is the command that gets executed inside the container, which in this case is to start the bash shell.
- Order of operations is 1. Pull (if needed): Get a pre-built image from a registry. 2. Run: Start a container from an image.


## Build
To build via automated CI, just push to `main`. See [`.gitlab-ci.yml`](.gitlab-ci.yml) for the runner definition.

To build locally for testing:

```
docker build --platform linux/amd64 --tag registry.gitlab.com/salk-tm/sleap-roots-predict:latest .
```

## Support
contact Elizabeth at eberrigan@salk.edu