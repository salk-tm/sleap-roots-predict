from pathlib import Path
import sys
import logging


class StreamToLogger:
    """Fake file-like stream object that redirects writes to a logger instance.

    Attributes:
        logger (logging.Logger): Logger object to which the stream is redirected.
        log_level (int): Logging level at which the messages are logged.
        linebuf (str): Buffer to hold the stream content before it's flushed to the logger.
    """

    def __init__(self, logger, log_level=logging.INFO):
        self.logger = logger
        self.log_level = log_level
        self.linebuf = ""

    def write(self, buf):
        """Writes the buffer 'buf' to the logger at the specified log level.

        Args:
            buf (str): String to be written to the logger.
        """
        for line in buf.rstrip().splitlines():
            self.logger.log(self.log_level, line.rstrip())

    def flush(self):
        """Flushes the stream.

        As the actual flushing is handled by the logger, this method is a placeholder
        to maintain the interface of a file-like stream.
        """
        pass


def setup_logging(
    output_dir,
    stdout_level=logging.INFO,
    stderr_level=logging.ERROR,
    capture_warnings=True,
):
    """Sets up the logging configuration.

    The function configures the root logger to write all messages to a file named
    'sleap-roots-predict.log' in the specified output directory. It also replaces
    sys.stdout and sys.stderr with instances of StreamToLogger to redirect all print
    statements and error messages to the logger. Debugging logs are also included
    to aid in debugging efforts.

    Args:
        output_dir (str): Directory where the log file will be saved.
        stdout_level (int, optional): Logging level for stdout. Defaults to logging.INFO.
        stderr_level (int, optional): Logging level for stderr. Defaults to logging.ERROR.
        capture_warnings (bool, optional): Whether to capture warnings and redirect them
            to the logging system.
    """
    log_file = Path(output_dir) / "sleap-roots-predict.log"

    # Configure the root logger to log to a file
    logging.basicConfig(
        level=logging.INFO,
        filename=log_file,
        format="%(asctime)s - %(levelname)s - %(message)s",
        filemode="w",  # Overwrite log file for each run
    )

    # Create logger instances for stdout and stderr
    stdout_logger = logging.getLogger("STDOUT")
    stderr_logger = logging.getLogger("STDERR")

    # Redirect stdout to the stdout logger
    sys.stdout = StreamToLogger(stdout_logger, stdout_level)

    # Redirect stderr to the stderr logger
    sys.stderr = StreamToLogger(stderr_logger, stderr_level)

    # Capture warnings if specified
    if capture_warnings:
        logging.captureWarnings(True)

    logging.info(f"Logging to {log_file}.")
