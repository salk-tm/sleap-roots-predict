from pathlib import Path
from typing import Tuple

import re
import logging
import sleap


def natural_sort(l):
    """https://stackoverflow.com/a/4836734"""
    l = [x.as_posix() if isinstance(x, Path) else x for x in l]
    convert = lambda text: int(text) if text.isdigit() else text.lower()
    alphanum_key = lambda key: [convert(c) for c in re.split("([0-9]+)", key)]
    return sorted(l, key=alphanum_key)


def modify_scan_path(scan_path: str, images_input_dir: str) -> str:
    """Modify the scan path to include the images input directory.

    Args:
        scan_path: The original scan path.
        images_input_dir: The directory containing the images.

    Returns:
        str: The modified scan path with the images input directory included.
    """
    # Convert to Path objects for easier manipulation
    scan_path = Path(scan_path)
    images_input_dir = Path(images_input_dir)

    # ./round1_2021/images/ -> {images_input_dir}/images/
    # Create the modified scan path by joining './', images_input_dir, and the 
    # remaining parts of scan_path starting from index 1
    modified_scan_path = Path(".", images_input_dir, *scan_path.parts[1:])

    return modified_scan_path.as_posix()


def video_from_scan(
    images_input_dir: str, scan_path: str, num_images: int
) -> sleap.Video:
    """Create a SLEAP Video object from a scan path.

    Args:
        scan_path: string pointing to the scan from "scans.csv".

    Returns:
        sleap.Video: A SLEAP Video object.
    """
    # Modify the scan path to include the images input directory in the container 
    # instead of the host (e.g., ./round1_2021/images/ -> /workspace/images_input/images/)
    container_scan_path = modify_scan_path(scan_path, images_input_dir)

    # Check if the scan path exists
    if Path(container_scan_path).exists():
        logging.info(f"Scan path {container_scan_path} exists.")

        # Get all of the image paths in the scan directory as strings in a list
        image_paths = [
            str(image_path) for image_path in Path(container_scan_path).glob("*.jpg")
        ]
        scan_image_paths = natural_sort(image_paths)
        logging.info(f"Found {len(scan_image_paths)} images in {container_scan_path}.")

        # Check if the number of images is correct
        if len(scan_image_paths) != num_images:
            logging.info(
                f"Number of images in scan{scan_path} is {len(scan_image_paths)}, expected {num_images}."
            )
            return None

        # Create a SLEAP Video object
        video = sleap.Video.from_image_filenames(scan_image_paths)
        logging.info(f"Created video from {scan_path}.")
        return video

    else:
        logging.info(f"Scan path {container_scan_path} does not exist.")
        return None


def load_models(models_input_dir: str, model_dict: dict) -> dict:
    """Load SLEAP models from the models input directory.

    Args:
        models_input_dir: Directory containing the models.
        model_dict: A dictionary where keys are model types and values are
            {'model_id': model_id, 'model_path': model_path}.

    Returns:
        dict: A dictionary where keys are model types and values 
            {'model_id': model_id, 'predictor': predictor} where predictor is a SLEAP 
            loaded model.
    """
    # Initialize dictionary for predictors
    predictors_dict = {}

    # Iterate over each model type
    for model_type, model_info in model_dict.items():
        model_id = model_info.get("model_id")
        model_path = model_info.get("model_path")

        # Modify model path to include the models input directory in the container
        modified_model_path = Path(models_input_dir) / model_path
        logging.info(
            f"Loading model {model_type} with model_id {model_id} from {modified_model_path}."
        )

        # Load the model
        predictor = sleap.load_model(
            modified_model_path.as_posix(), progress_reporting="none"
        )
        logging.info(f"Loaded model {model_type} from {modified_model_path.as_posix()}.")

        # Add predictor to dictionary
        predictors_dict[model_type] = {"model_id": model_id, "predictor": predictor}

    return predictors_dict


def predict(
    video: sleap.Video,
    scan_id: str,
    predictors_dict: dict,
    output_dir: str,
) -> Tuple[sleap.Labels, dict]:
    """Get the SLEAP predictions.

    Args:
        video: A SLEAP Video object.
        scan_id: Unique ID of scan for naming the predictions.
        predictors_dict: A dictionary where keys are model types and values are 
            {'model_id': model_id, 'predictor': predictor}.
        output_dir: Directory to save predictions and predictions.csv.

    Returns:
        sleap.Labels: SLEAP predictions.
        dict: A dictionary containing the scan_id and the prediction names for 
            each model type: {'scan_id': scan_id, 'model_type': preds_name}.
    """
    # Initialize dictionary for prediction info
    preds_dict = {}
    # Add scan_id to dictionary
    preds_dict["scan_id"] = scan_id

    # Check if video is None
    if video is None:
        return None, preds_dict

    # Log sleap version
    logging.info(f"SLEAP version: {sleap.versions()}")

    # Iterate over each model type
    for model_type, model_info in predictors_dict.items():
        model_id = model_info.get("model_id")
        predictor = model_info.get("predictor")

        # Build path to save predictions
        preds_name = f"scan_{scan_id}.model_{model_id}.root_{model_type}.slp"
        preds_path = Path(output_dir) / preds_name

        # Get the predictions
        labels = predictor.predict(video)
        # Save the predictions
        labels.save(preds_path)
        logging.info(f"Saved predictions to {preds_path.as_posix()}.")

        # Add prediction name to dictionary (path in output_dir)
        preds_dict[model_type] = preds_name

    return labels, preds_dict
