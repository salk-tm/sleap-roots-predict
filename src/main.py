from pathlib import Path

import logging
import sys
import pandas as pd
import json

from src.log_util import setup_logging, StreamToLogger
from src.predict import load_models, video_from_scan, predict


def main(images_input_dir: str, models_input_dir: str, output_dir: str) -> pd.DataFrame:
    """Get the SLEAP predictions.

    Args:
        images_input_dir: Contains scan images and scans.csv.
        models_input_dir: Contains models and model_paths.csv.
        output_dir: Directory to save predictions and predictions.csv.

    Returns:
        pd.DataFrame: scan id, model id, root type, prediction path
    """
    # Make Path objects
    images_input_dir = Path(images_input_dir)
    models_input_dir = Path(models_input_dir)
    output_dir = Path(output_dir)

    # Set up logging
    setup_logging(output_dir)

    scans_csv = images_input_dir / "scans.csv"
    model_paths_csv = models_input_dir / "model_paths.csv"
    logging.info(f"Reading scan paths from {scans_csv}.")
    logging.info(f"Reading model paths from {model_paths_csv}.")

    # Read csv with path info
    scans_df = pd.read_csv(scans_csv)  # "scan_path" "scan_id"

    # Get dataset parameters
    logging.info(f"Getting dataset parameters from {images_input_dir}.")
    params = get_params(images_input_dir)
    species = params.get("species_name")
    logging.info(f"Species: {species}")
    min_wave = params.get("min_wave")
    max_wave = params.get("max_wave")
    logging.info(f"Wave range: {min_wave} to {max_wave}")
    min_age = params.get("min_age")
    max_age = params.get("max_age")
    logging.info(f"Age range: {min_age} to {max_age}")

    # Filter scans based on dataset parameters
    if species:
        scans_df = scans_df[scans_df["species_name"] == species]
    if min_wave is not None:
        scans_df = scans_df[scans_df["wave_number"] >= min_wave]
    if max_wave is not None:
        scans_df = scans_df[scans_df["wave_number"] <= max_wave]
    if min_age is not None:
        scans_df = scans_df[scans_df["plant_age_days"] >= min_age]
    if max_age is not None:
        scans_df = scans_df[scans_df["plant_age_days"] <= max_age]

    logging.info(f"Filtered scans has a shape of {scans_df.shape}.")

    # Get model paths
    model_dict = parse_model_csv(model_paths_csv)
    logging.info(f"Model paths: {model_dict}")
    # Get scan paths
    scan_paths = scans_df["scan_path"].tolist()

    # Initialize a list for SLEAP Video objects
    video_list = []
    # Iterate over each scan
    for scan_path in scan_paths:
        # Initialize dictionary for video info
        video_dict = {}
        logging.info(f"Processing scan: {scan_path}")
        scan_id = scans_df[scans_df["scan_path"] == scan_path]["scan_id"].values[0]
        logging.info(f"Scan ID: {scan_id}")

        # Create a SLEAP Video object
        video = video_from_scan(images_input_dir, scan_path, num_images=72)
        if video is not None:
            video_dict["scan_id"] = scan_id
            video_dict["video"] = video
            video_list.append(video_dict)
        else:
            logging.info(f"Skipping scan {scan_path}.")

    # Use model_dict to load models once and pass to predict
    predictors_dict = load_models(models_input_dir, model_dict)

    # Initialize a list for prediction info
    preds_list = []
    for dict in video_list:
        # Check if video key exists
        if "video" not in dict:
            logging.info(f"Skipping scan {dict['scan_id']} since video does not exist.")
            continue

        # Get the video and scan_id
        video = dict.get("video")
        scan_id = dict.get("scan_id")

        # Get the SLEAP predictions
        try:
            labels_dict = predict(
                video, scan_id, predictors_dict, output_dir
            )[1]
            preds_list.append(labels_dict)
        except Exception as e:
            logging.error(f"Error processing scan {scan_path}: {e}")
            continue

    # Save predictions info
    preds_df = pd.DataFrame(preds_list)
    # Merge with scans_df
    preds_scans_df = pd.merge(scans_df, preds_df, on="scan_id", how="left")
    preds_scans_df.to_csv(output_dir / "predictions.csv", index=False)
    logging.info(f"Saved predictions to {output_dir / 'predictions.csv'}.")
    logging.info("Done.")
    return preds_scans_df


def parse_model_csv(csv_file_path: str) -> dict:
    """Parses a CSV file containing model information and returns a dictionary.

    Args:
        csv_file_path: The file path to the CSV file.

    Returns:
        dict: A dictionary where keys are model types and values are dictionaries
            containing 'model_id' and 'model_path' for each model type.
    """
    model_dict = {}
    # Read CSV file into a pandas DataFrame
    df = pd.read_csv(csv_file_path)

    # Iterate over each row in the DataFrame
    for index, row in df.iterrows():
        model_type = row["model_type"]
        model_path = row["model_path"]
        # For now, model_id is name from model_path without the .zip extension
        model_id = Path(model_path).stem
        logging.info(f"model_id: {model_id}")
        model_dict[model_type] = {"model_id": model_id, "model_path": model_path}
    return model_dict


def get_params(input_dir: str) -> dict:
    """Read the parameters from the `dataset_params.json` in images-input-directory.

    Args:
        input_dir: Path to the input directory. This directory should contain
            a `dataset_params.json` file which is used to filter the dataset processed
            using `scans.csv`.

    Returns:
        dict: A dictionary containing the identifiying characteristics for a dataset.

    """
    params = {}
    params_file = Path(input_dir) / "dataset_params.json"
    if params_file.exists():
        with open(params_file, "r") as f:
            params = json.load(f)
        return params
    else:
        raise FileNotFoundError(f"dataset_params.json not found in {input_dir}")


if __name__ == "__main__":
    images_input_dir = sys.argv[1]
    models_input_dir = sys.argv[2]
    output_dir = sys.argv[3]
    main(images_input_dir, models_input_dir, output_dir)
