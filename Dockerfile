# TensorFlow 2.7.0 base image with SLEAP installed
FROM registry.gitlab.com/salk-tm/sleap-train:latest

USER root

WORKDIR /workspace
COPY ./src /workspace/src

# Set the PYTHONPATH environment variable to include the parent directory of the package
# This helps imports
ENV PYTHONPATH=/workspace:$PYTHONPATH

# Due to this "WARNING:matplotlib:Matplotlib created a temporary cache directory..."
# ..."it is highly recommended to set the MPLCONFIGDIR environment variable to a" 
# "writable directory, in particular to speed up the import of Matplotlib and to" 
# "better support multiprocessing."
# Create the directory with read/write permissions
RUN mkdir -p /workspace/temp/matplotlib && chmod a+rw /workspace/temp/matplotlib
# Set the MPLCONFIGDIR environment variable to the directory
ENV MPLCONFIGDIR=/workspace/temp/matplotlib
RUN echo $MPLCONFIGDIR
